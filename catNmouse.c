///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date    20_01_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Define all the variables up above
#define DEFAULT_MAX_NUMBER (2048)

int main( int argc, char* argv[] ) {

   int maxValue;
   int aGuess = -1; // Set the value to -1 to make sure that it falls within the while statment
   int randomNumber;

   // sets the maxValue based on how many arguments were given when ran
   if (argc != 1){
      if (atoi(argv[1]) < 1){
         return 1; // returns a 1 if the value is < 1
      }
      maxValue = atoi(argv[1]);
   }
   else{
      maxValue = DEFAULT_MAX_NUMBER;
   }

   // Generates a random number from 1 to the maxValue
   srand(time(NULL));
   randomNumber = (rand() % maxValue) + 1;

   // Asks for a input until it reaches the randomNumber
   // if user inputs a value not in the range it will print the according statement
   // if the users guess is off then it will state if it is greater than or less than the number they gave
   do
   {
      // Ask User for a guess
      printf("OK cat, I’m thinking of a number from 1 to %d. Make a guess: \n", maxValue);
      scanf("%d", &aGuess);
      if (aGuess < 1){
         printf("You must enter a number that's >= 1\n");
      }
      else if (aGuess > maxValue){
         printf("You must enter a number that's <= %d\n", maxValue);
      }
      else if (aGuess > randomNumber) {
         printf("No cat... the number I’m thinking of is smaller than %d\n", aGuess);
      }
      else if (aGuess < randomNumber){
         printf("No cat... the number I’m thinking of is larger than %d\n", aGuess);
      }

   } while(aGuess != randomNumber);
   printf("You got me\n");
   printf("  /\\_/\\ \n ( o o ) \n ==_Y_== \n `-'\n");

   return 0;  // This is an example of how to return a 0
}

